package logic;

import data.AbstractDAO;
import data.ClientDAO;
import logic.validators.EmailValidator;
import logic.validators.Validator;
import model.Client;

/**
 * Business Logic Layer for Client entities
 */
public class ClientBLL {
    private Validator<Client> validator;
    public ClientBLL(){
        validator = new EmailValidator();
    }
    public Client insert(Client client){
        AbstractDAO<Client> clientAbstractDAO = new ClientDAO();
        validator.validate(client);
        return clientAbstractDAO.insert(client);
    }
}
