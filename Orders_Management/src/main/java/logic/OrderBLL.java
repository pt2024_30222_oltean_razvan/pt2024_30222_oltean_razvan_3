package logic;

import data.AbstractDAO;
import data.OrderDAO;
import logic.validators.*;
import model.Order;

/**
 * Business Logic Layer for Order entities
 */
public class OrderBLL{
    private Validator<Order> validator1;
    private Validator<Order> validator2;
    private Validator<Order> validator3;
    public OrderBLL(){
        validator1 = new NegativeOrderQuantityValidator();
        validator2 = new UnderstockValidator();
        validator3 = new PriceValidator();
    }
    public Order insert(Order order,int quantity) throws UnderstockException {
        AbstractDAO<Order> orderAbstractDAO = new OrderDAO();
        validator1.validate(order);
        validator2.validate(order, quantity);
        validator3.validate(order);
        return orderAbstractDAO.insert(order);
    }
}
