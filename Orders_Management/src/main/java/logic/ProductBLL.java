package logic;

import data.AbstractDAO;
import data.ProductDAO;
import logic.validators.NegativeQuantityValidator;
import logic.validators.Validator;
import model.Product;

/**
 * Business Logic Layer for Product entities
 */
public class ProductBLL {
    private Validator<Product> validator;
    public ProductBLL(){
        validator = new NegativeQuantityValidator();
    }
    public Product insert(Product product){
        AbstractDAO<Product> productAbstractDAO = new ProductDAO();
        validator.validate(product);
        return productAbstractDAO.insert(product);
    }
}
