package logic.validators;

import model.Product;
/**
 * Validator class
 */
public class NegativeQuantityValidator implements Validator<Product>{
    @Override
    public void validate(Product product) {
        if(product.getQuantity() <= 0){
            throw new IllegalArgumentException();
        }
    }
    @Override
    public void validate(Product product, int productQuantity) {

    }
}
