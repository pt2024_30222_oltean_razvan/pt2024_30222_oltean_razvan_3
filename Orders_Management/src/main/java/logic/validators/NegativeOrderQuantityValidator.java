package logic.validators;

import model.Order;
/**
 * Validator class
 */
public class NegativeOrderQuantityValidator implements Validator<Order> {
    @Override
    public void validate(Order order) {
        if(order.getQuantity() <= 0){
            throw new IllegalArgumentException();
        }
    }
    @Override
    public void validate(Order order, int productQuantity) {}
}
