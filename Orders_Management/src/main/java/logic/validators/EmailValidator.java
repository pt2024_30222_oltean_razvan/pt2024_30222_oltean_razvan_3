package logic.validators;

import model.Client;
import model.Order;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Validator class
 */
public class EmailValidator implements Validator<Client>{
    @Override
    public void validate(Client client) {
        String EMAIL = "[a-zA-z]+@[a-zA-z]+\\.[a-zA-z]+";
        Pattern pattern = Pattern.compile(EMAIL);
        if(!pattern.matcher(client.getEmail()).matches()){
            throw new IllegalArgumentException();
        }
    }
    @Override
    public void validate(Client client, int productQuantity) {}
}
