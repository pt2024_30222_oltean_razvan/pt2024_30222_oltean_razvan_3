package logic.validators;

import model.Order;
/**
 * Validator class
 */
public class PriceValidator implements Validator<Order>{
    @Override
    public void validate(Order order){
        if(order.getPrice() < 0){
            throw new IllegalArgumentException();
        }
    }
    @Override
    public void validate(Order order, int productQuantity){}
}
