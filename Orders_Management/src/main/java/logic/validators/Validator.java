package logic.validators;

public interface Validator<T> {
    void validate(T t);
    public void validate(T t,int productQuantity) throws UnderstockException;
}
