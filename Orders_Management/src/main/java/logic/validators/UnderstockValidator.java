package logic.validators;

import model.Order;
import model.Product;
/**
 * Validator class
 */
public class UnderstockValidator implements Validator<Order>{
    @Override
    public void validate(Order order, int productQuantity) throws UnderstockException {
        if(order.getQuantity() > productQuantity){
            throw new UnderstockException();
        }
    }

    @Override
    public void validate(Order order) {}
}
