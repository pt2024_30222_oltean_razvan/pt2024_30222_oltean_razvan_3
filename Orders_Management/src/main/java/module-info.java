module com.example.orders_management {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.sql;
    requires java.desktop;
    requires java.base;

    exports view;
    opens view to javafx.fxml;
    exports com.example.orders_management;
    opens com.example.orders_management to javafx.fxml;
    exports model;
    opens model to javafx.fxml;
    exports data;
    opens data to javafx.fxml;
    exports logic;
    opens logic to javafx.fxml;
}