package model;

/**
 * The Order class represents an order entity.
 */
public class Order {

    /**
     * The Bill record represents a bill associated with an order.
     */
    record Bill(int id, double price){}

    private int id;
    private int clientId;
    private int productId;
    private int quantity;
    private Bill bill;
    private String clientName;
    private String productName;

    /**
     * Default constructor for Order.
     */
    public Order() {}

    /**
     * Constructs an Order with specified quantity, price, client name, and product name.
     *
     * @param quantity the quantity of the product
     * @param price the price of the order
     * @param clientName the name of the client
     * @param productName the name of the product
     */
    public Order(int quantity, int price, String clientName, String productName) {
        this.quantity = quantity;
        this.clientName = clientName;
        this.productName = productName;
    }

    /**
     * Retrieves the client name associated with the order.
     *
     * @return the client name
     */
    public String getClientName() {
        return clientName;
    }

    /**
     * Sets the client name associated with the order.
     *
     * @param clientName the client name to set
     */
    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    /**
     * Retrieves the product name associated with the order.
     *
     * @return the product name
     */
    public String getProductName() {
        return productName;
    }

    /**
     * Sets the product name associated with the order.
     *
     * @param productName the product name to set
     */
    public void setProductName(String productName) {
        this.productName = productName;
    }

    /**
     * Retrieves the ID of the order.
     *
     * @return the ID of the order
     */
    public int getId() {
        return id;
    }

    /**
     * Sets the ID of the order.
     *
     * @param id the ID to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Retrieves the ID of the client associated with the order.
     *
     * @return the ID of the client
     */
    public int getClientId() {
        return clientId;
    }

    /**
     * Sets the ID of the client associated with the order.
     *
     * @param clientId the ID of the client to set
     */
    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    /**
     * Retrieves the ID of the product associated with the order.
     *
     * @return the ID of the product
     */
    public int getProductId() {
        return productId;
    }

    /**
     * Sets the ID of the product associated with the order.
     *
     * @param productId the ID of the product to set
     */
    public void setProductId(int productId) {
        this.productId = productId;
    }

    /**
     * Retrieves the quantity of the product in the order.
     *
     * @return the quantity of the product
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * Sets the quantity of the product in the order.
     *
     * @param quantity the quantity of the product to set
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    /**
     * Retrieves the price of the order.
     *
     * @return the price of the order
     */
    public double getPrice(){
        return bill.price();
    }

    /**
     * Retrieves the log ID associated with the order's bill.
     *
     * @return the log ID
     */
    public int getLogId(){
        return bill.id();
    }

    /**
     * Sets the bill for the order with the specified ID and price.
     *
     * @param id the ID of the bill
     * @param price the price of the bill
     */
    public void setBill(int id, double price){
        bill = new Bill(id, price);
    }
}
