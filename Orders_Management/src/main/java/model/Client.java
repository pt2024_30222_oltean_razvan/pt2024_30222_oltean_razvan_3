package model;

/**
 * The Client class represents a client entity.
 */
public class Client {
    private int id;
    private String name;
    private String email;
    /**
     * Constructs a new Client with default values.
     */
    public Client() {}

    /**
     * Constructs a new Client with the specified id, name, and email.
     *
     * @param id    the unique identifier for the client
     * @param name  the name of the client
     * @param email the email address of the client
     */
    public Client(int id, String name, String email) {
        this.id = id;
        this.name = name;
        this.email = email;
    }
    /**
     * Retrieves the id of the client.
     * @return The id of the client.
     */
    public int getId() {
        return id;
    }

    /**
     * Sets the ID of the client.
     * @param id The ID to set.
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Retrieves the name of the client.
     * @return The name of the client.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name of the client.
     * @param name The name to set.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Retrieves the email of the client.
     * @return The email of the client.
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets the email of the client.
     * @param email The email to set.
     */
    public void setEmail(String email) {
        this.email = email;
    }
}
