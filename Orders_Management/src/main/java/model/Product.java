package model;

/**
 * The Product class represents a product entity.
 */
public class Product {
    private int id;
    private String name;
    private int quantity;
    /**
     * Constructs a new Product with default values.
     */
    public Product() {}

    /**
     * Constructs a new Product with the specified id, name, and quantity.
     *
     * @param id       the unique identifier for the product
     * @param name     the name of the product
     * @param quantity the quantity of the product in stock
     */
    public Product(int id, String name, int quantity) {
        this.id = id;
        this.name = name;
        this.quantity = quantity;
    }


    /**
     * Retrieves the ID of the product.
     * @return The ID of the product.
     */
    public int getId() {
        return id;
    }

    /**
     * Sets the ID of the product.
     * @param id The ID to set.
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Retrieves the name of the product.
     * @return The name of the product.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name of the product.
     * @param name The name to set.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Retrieves the quantity of the product.
     * @return The quantity of the product.
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * Sets the quantity of the product.
     * @param quantity The quantity to set.
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
