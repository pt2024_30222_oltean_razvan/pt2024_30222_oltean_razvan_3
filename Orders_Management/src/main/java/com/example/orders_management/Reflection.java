package com.example.orders_management;

import java.lang.reflect.Field;

/**
 * The Reflection class provides utility methods for working with Java Reflection API.
 * This class contains a method to retrieve and print the properties of an object.
 */
public class Reflection {

    /**
     * Retrieves and prints the properties (fields) of the given object.
     * This method uses Java Reflection to access the declared fields of the object's class,
     * including private fields, and prints their names and values.
     *
     * @param object the object whose properties are to be retrieved and printed.
     *               If the object is null, a NullPointerException will be thrown.
     */
    public static void retrieveProperties(Object object) {

        for (Field field : object.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            Object value;
            try {
                value = field.get(object);
                System.out.println(field.getName() + "=" + value);

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }
}
