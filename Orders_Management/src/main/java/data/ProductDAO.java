package data;

import model.Product;

/**
 * Data Access Object for Product entities.
 */
public class ProductDAO extends AbstractDAO<Product> {
}
