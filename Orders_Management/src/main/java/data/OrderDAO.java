package data;

import model.Order;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
/**
 * Data Access Object for the Order model.
 */
public class OrderDAO extends AbstractDAO<Order> {
    /**
     * Retrieves all orders from the database.
     *
     * @return a list of all orders
     */
    public List<Order> findAll() {
        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        String executeQuery = "SELECT * FROM view_order";
        List<Order> resultList = new ArrayList<>();
        try {
            preparedStatement = connection.prepareStatement(executeQuery);
            resultSet = preparedStatement.executeQuery();

            resultList = createObjects(resultSet);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            databaseConnection.close(connection, null, resultSet, null, preparedStatement);
        }
        return resultList;
    }
    private List<Order> createObjects(ResultSet resultSet) {
        List<Order> list = new ArrayList<>();
        Constructor[] constructors = Order.class.getDeclaredConstructors();
        Constructor constructor = null;

        for (int i = 0; i < constructors.length; i++) {
            constructor = constructors[i];
            if (constructor.getGenericParameterTypes().length == 0)
                break;
        }
        try {
            while (resultSet.next()) {
                constructor.setAccessible(true);
                Order instance = (Order) constructor.newInstance();
                for (Field field : Order.class.getDeclaredFields()) {
                    String fieldName = field.getName();
                    Object value = 0;
                    try {
                        if(!fieldName.equals("bill")) {
                            value = resultSet.getObject(fieldName);
                            PropertyDescriptor propertyDescriptor = new PropertyDescriptor(fieldName, Order.class);
                            Method method = propertyDescriptor.getWriteMethod();
                            method.invoke(instance, value);
                        }else{
                            value = resultSet.getObject("price");
                            Object valueId = resultSet.getObject("id");
                            Method method = Order.class.getMethod("setBill", int.class, double.class);
                            method.invoke(instance,(int) valueId,(double) value);
                        }
                    }catch (SQLException ex){
                        //skip
                    }
                }
                list.add(instance);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return list;
    }
    /**
     * Inserts a new order into the database.
     *
     * @param order the order to be inserted
     * @return the inserted order
     */
    public Order insert(Order order)
    {
        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();
        PreparedStatement preparedStatement = null;
        try
        {
            String executeLogQuery = createInsertLogQuery(order);
            String executeOrderQuery = createInsertQuery(order);
            preparedStatement = connection.prepareStatement(executeLogQuery);
            setInsertLogStatementParameters(preparedStatement, order);
            preparedStatement.executeUpdate();
            preparedStatement = connection.prepareStatement(executeOrderQuery);
            setInsertStatementParameters(preparedStatement, order);
            preparedStatement.executeUpdate();
        }
        catch (Exception ex){
            ex.printStackTrace();
        }
        finally
        {
            databaseConnection.close(connection,null,null,null,preparedStatement);
        }
        return order;
    }
    private String createInsertLogQuery(Order order) throws IntrospectionException
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("INSERT INTO `log` (");

        stringBuilder.append("log_id, order_price");
        stringBuilder.append(") VALUES (");
        stringBuilder.append("?, ?");

        stringBuilder.append(")");
        return stringBuilder.toString();
    }
    private String createInsertQuery(Order order) throws IntrospectionException
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("INSERT INTO `order` (");
        stringBuilder.append("order_id, client_id, product_id, log_id, product_quantity");
        stringBuilder.append(") VALUES (");

        stringBuilder.append("?, ?, ?, ?, ?");
        stringBuilder.append(")");
        return stringBuilder.toString();
    }
    private void setInsertStatementParameters(PreparedStatement preparedStatement, Order order) throws IntrospectionException, SQLException, IllegalAccessException, InvocationTargetException
    {
        preparedStatement.setObject(1, order.getId());
        preparedStatement.setObject(2, order.getClientId());
        preparedStatement.setObject(3, order.getProductId());
        preparedStatement.setObject(4, order.getLogId());
        preparedStatement.setObject(5, order.getQuantity());
    }
    private void setInsertLogStatementParameters(PreparedStatement preparedStatement, Order order) throws IntrospectionException, SQLException, IllegalAccessException, InvocationTargetException
    {
        preparedStatement.setObject(1, order.getLogId());
        preparedStatement.setObject(2, order.getPrice());
    }
}
