package data;

import java.beans.*;
import java.lang.reflect.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
/**
 * The AbstractDAO class provides a generic Data Access Object (DAO) with common database operations.
 * This class is meant to be extended by specific DAO implementations for various entity types.
 *
 * @param <T> the type of the entity
 */
public abstract class AbstractDAO<T> {
    private final Class<T> type;
    /**
     * Constructs an AbstractDAO, initializing the type parameter.
     */
    @SuppressWarnings("unchecked")
    public AbstractDAO() {
        this.type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }
    private String createInterrogationQuery(String field) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("SELECT * FROM view_");
        stringBuilder.append(type.getSimpleName().toLowerCase());
        if (!field.isEmpty()) {
            stringBuilder.append(" WHERE ").append(field).append(" like ?");
        }
        return stringBuilder.toString();
    }
    /**
     * Finds and returns all entities from the database.
     *
     * @return a list of all entities
     */
    public List<T> findAll() {
        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        String executeQuery = createInterrogationQuery("");
        List<T> resultList = new ArrayList<>();
        try {
            preparedStatement = connection.prepareStatement(executeQuery);
            resultSet = preparedStatement.executeQuery();

            resultList = createObjects(resultSet);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            databaseConnection.close(connection, null, resultSet, null, preparedStatement);
        }
        return resultList;
    }
    /**
     * Finds and returns all entities that match the given name from the database.
     *
     * @param name the name to match
     * @return a list of matching entities
     */
    public List<T> findAll(String name) {
        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        String executeQuery = createInterrogationQuery(getColumnProperty("name"));
        List<T> resultList = new ArrayList<>();
        try {
            preparedStatement = connection.prepareStatement(executeQuery);
            preparedStatement.setString(1, "%" + name + "%");
            resultSet = preparedStatement.executeQuery();

            resultList = createObjects(resultSet);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            databaseConnection.close(connection, null, resultSet, null, preparedStatement);
        }
        return resultList;
    }
    private String createSelectQuery() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("SELECT * FROM view_");
        stringBuilder.append(type.getSimpleName().toLowerCase());
        stringBuilder.append(" WHERE ").append(getColumnProperty("name")).append(" =?");
        return stringBuilder.toString();
    }
    private String getColumnProperty(String property) {
        return type.getSimpleName().toLowerCase() + "_" + property;
    }
    /**
     * Finds and returns an entity by its name from the database.
     *
     * @param name the name of the entity
     * @return the entity with the given name, or {@code null} if not found
     * @throws IllegalArgumentException if no entity with the given name is found
     */
    public T findByName(String name) {
        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        String executeQuery = createSelectQuery();
        try {
            preparedStatement = connection.prepareStatement(executeQuery);
            preparedStatement.setString(1, name);
            resultSet = preparedStatement.executeQuery();

            List<T> resultObjects = createObjects(resultSet);
            if (!resultObjects.isEmpty()) {
                return resultObjects.get(0);
            } else {
                throw new IllegalArgumentException(type.getSimpleName() + " with NAME " + name + " does not exist!");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            databaseConnection.close(connection, null, resultSet, null, preparedStatement);
        }
        return null;
    }
    /**
     * Finds and returns the total number of entities in the database.
     *
     * @return the total number of entities
     */
    public int findSize(){
        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        int size = 0;
        String executeQuery = createInterrogationQuery("");
        try {
            preparedStatement = connection.prepareStatement(executeQuery);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) size++;
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            databaseConnection.close(connection, null, resultSet, null, preparedStatement);
        }
        return size;
    }
    /**
     * Finds and returns the last ID of the entities in the database.
     *
     * @return the last ID
     */
    public int findLastId(){
        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        int id = 0;
        String executeQuery = "SELECT * FROM `" + type.getSimpleName() + "`";
        try {
            preparedStatement = connection.prepareStatement(executeQuery);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) id = resultSet.getInt(getColumnProperty("id"));
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            databaseConnection.close(connection, null, resultSet, null, preparedStatement);
        }
        return id;
    }
    private List<T> createObjects(ResultSet resultSet) {
        List<T> list = new ArrayList<>();
        Constructor[] constructors = type.getDeclaredConstructors();
        Constructor constructor = null;

        for (int i = 0; i < constructors.length; i++) {
            constructor = constructors[i];
            if (constructor.getGenericParameterTypes().length == 0)
                break;
        }
        try {
            while (resultSet.next()) {
                constructor.setAccessible(true);
                T instance = (T) constructor.newInstance();
                for (Field field : type.getDeclaredFields()) {
                    String fieldName = field.getName();
                    Object value = resultSet.getObject(getColumnProperty(fieldName));
                    PropertyDescriptor propertyDescriptor = new PropertyDescriptor(fieldName, type);
                    Method method = propertyDescriptor.getWriteMethod();
                    method.invoke(instance, value);
                }
                list.add(instance);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return list;
    }
    /**
     * Inserts the given entity into the database.
     *
     * @param t the entity to insert
     * @return the inserted entity
     */
    public T insert(T t)
    {
        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();
        PreparedStatement preparedStatement = null;
        try
        {
            String executeQuery = createInsertQuery(t);
            preparedStatement = connection.prepareStatement(executeQuery);
            setInsertStatementParameters(preparedStatement, t);
            preparedStatement.executeUpdate();
        }
        catch (Exception ex){
            ex.printStackTrace();
        }
        finally
        {
            databaseConnection.close(connection,null,null,null,preparedStatement);
        }
        return t;
    }
    private String createInsertQuery(T t) throws IntrospectionException
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("INSERT ");
        stringBuilder.append("INTO ");
        stringBuilder.append(type.getSimpleName());
        stringBuilder.append(" (");

        BeanInfo beanInfo = Introspector.getBeanInfo(t.getClass());
        PropertyDescriptor[] propertyDescriptors = beanInfo.getPropertyDescriptors();
        for (PropertyDescriptor propertyDescriptor : propertyDescriptors)
        {
            if (!propertyDescriptor.getName().equals("class"))
            {
                stringBuilder.append(type.getSimpleName()).append("_").append(propertyDescriptor.getName()).append(", ");
            }
        }
        stringBuilder.delete(stringBuilder.length() - 2, stringBuilder.length());
        stringBuilder.append(") VALUES (");

        stringBuilder.append("?, ".repeat(Math.max(0, propertyDescriptors.length - 1)));
        stringBuilder.delete(stringBuilder.length() - 2, stringBuilder.length());
        stringBuilder.append(")");
        return stringBuilder.toString();
    }
    private void setInsertStatementParameters(PreparedStatement preparedStatement, T t) throws IntrospectionException, SQLException, IllegalAccessException, InvocationTargetException
    {
        BeanInfo beanInfo = Introspector.getBeanInfo(t.getClass());
        PropertyDescriptor[] propertyDescriptors = beanInfo.getPropertyDescriptors();
        int i = 1;
        for (PropertyDescriptor propertyDescriptor : propertyDescriptors)
        {
            if (!propertyDescriptor.getName().equals("class"))
            {
                Method method = propertyDescriptor.getReadMethod();
                Object value = method.invoke(t);
                preparedStatement.setObject(i++, value);
            }
        }
    }
    /**
     * Updates the given entity in the database.
     *
     * @param t the entity to update
     * @return the updated entity
     */
    public T update(T t)
    {
        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();
        PreparedStatement preparedStatement = null;
        try
        {
            String executeQuery = createUpdateQuery(t);
            preparedStatement = connection.prepareStatement(executeQuery);

            setUpdateStatementParameters(preparedStatement, t);
            setWhereParameter(preparedStatement, t);

            preparedStatement.executeUpdate();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        finally
        {
            databaseConnection.close(connection,null,null,null,preparedStatement);
        }
        return t;
    }
    private String createUpdateQuery(T t) throws IntrospectionException
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("UPDATE ").append(type.getSimpleName()).append(" SET ");

        BeanInfo beanInfo = Introspector.getBeanInfo(t.getClass());
        PropertyDescriptor[] propertyDescriptors = beanInfo.getPropertyDescriptors();
        for (PropertyDescriptor propertyDescriptor : propertyDescriptors)
        {
            String propertyName = propertyDescriptor.getName();
            if (!propertyName.equals("class") && !propertyName.equals("id"))
            {
                stringBuilder.append(getColumnProperty(propertyName)).append("=?, ");
            }
        }
        stringBuilder.delete(stringBuilder.length() - 2, stringBuilder.length());
        stringBuilder.append(" WHERE ").append(getColumnProperty("id")).append(" =?");
        return stringBuilder.toString();
    }
    private void setUpdateStatementParameters(PreparedStatement statement, T t) throws IntrospectionException, SQLException, IllegalAccessException, InvocationTargetException
    {
        BeanInfo beanInfo = Introspector.getBeanInfo(t.getClass());
        PropertyDescriptor[] propertyDescriptors = beanInfo.getPropertyDescriptors();
        int i = 1;
        for (PropertyDescriptor propertyDescriptor : propertyDescriptors)
        {
            if (!propertyDescriptor.getName().equals("class") && !propertyDescriptor.getName().equals("id"))
            {
                Method method = propertyDescriptor.getReadMethod();
                Object value = method.invoke(t);
                statement.setObject(i++, value);
            }
        }
    }
    private void setWhereParameter(PreparedStatement statement, T t) throws IntrospectionException, SQLException, IllegalAccessException, InvocationTargetException
    {
        BeanInfo beanInfo = Introspector.getBeanInfo(t.getClass());
        PropertyDescriptor[] propertyDescriptors = beanInfo.getPropertyDescriptors();
        for (PropertyDescriptor propertyDescriptor : propertyDescriptors)
        {
            if (propertyDescriptor.getName().equals("id"))
            {
                Method method = propertyDescriptor.getReadMethod();
                int id = (int) method.invoke(t);
                statement.setInt(propertyDescriptors.length-1, id);
                break;
            }
        }
    }
    private String createDeleteQuery(){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("DELETE FROM ");
        stringBuilder.append(type.getSimpleName());
        stringBuilder.append(" WHERE ");
        stringBuilder.append(getColumnProperty("id"));
        stringBuilder.append(" =?");
        return stringBuilder.toString();
    }
    /**
     * Deletes the given entity from the database.
     *
     * @param t the entity to delete
     */
    public void delete(T t)
    {
        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();
        PreparedStatement preparedStatement = null;
        try
        {
            String executeQuery = createDeleteQuery();
            String executeOrderQuery = "DELETE FROM `order` where " + getColumnProperty("id") + " =?";
            preparedStatement = connection.prepareStatement(executeOrderQuery);


            PropertyDescriptor propertyDescriptor = new PropertyDescriptor("id", type);
            Method method = propertyDescriptor.getReadMethod();
            Object value = method.invoke(t);

            preparedStatement.setInt(1, (int) value);
            preparedStatement.executeUpdate();
            preparedStatement = connection.prepareStatement(executeQuery);
            preparedStatement.setInt(1, (int) value);
            preparedStatement.executeUpdate();
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        finally
        {
            databaseConnection.close(connection, null, null, null, preparedStatement);
        }
    }
}