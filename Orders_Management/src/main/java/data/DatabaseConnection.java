package data;

import java.sql.*;

/**
 * The DatabaseConnection class manages the connection to the database.
 */
public class DatabaseConnection {
    /**
     * The databaseLink connection object.
     */
    public Connection databaseLink;

    /**
     * Establishes a connection to the database.
     * @return The database connection.
     */
    public Connection getConnection(){
        String url = "jdbc:mysql://localhost:3306/orders_management";
        String user = "root";
        String pass = "Cartof1234!";

        try{
            Class.forName("com.mysql.cj.jdbc.Driver");
            databaseLink = DriverManager.getConnection(url, user, pass);
        }catch (Exception e){
            e.printStackTrace();
        }
        return databaseLink;
    }

    /**
     * Closes the database connection, statement, result set, and callable statement.
     * @param connection The connection to be closed.
     * @param statement The statement to be closed.
     * @param resultSet The result set to be closed.
     * @param callableStatement The callable statement to be closed.
     */
    public void close(Connection connection, Statement statement, ResultSet resultSet, CallableStatement callableStatement,PreparedStatement preparedStatement){
        try {
            if(connection != null)
                connection.close();
            if(statement != null)
                statement.close();
            if(resultSet != null)
                resultSet.close();
            if(callableStatement != null)
                callableStatement.close();
            if(preparedStatement != null)
                preparedStatement.close();
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
}
