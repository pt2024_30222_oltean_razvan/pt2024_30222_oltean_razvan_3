package view;

import com.example.orders_management.Main;
import data.AbstractDAO;
import data.ProductDAO;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import logic.ProductBLL;
import model.Product;

/**
 * The AddProductController class controls the functionality of the Add Product view.
 */
public class AddProductController {

    @FXML
    private Button addButton;

    @FXML
    private Button backButton;

    @FXML
    private TextField quantityField;

    @FXML
    private Label quantityLabel;

    @FXML
    private TextField nameField;

    @FXML
    private Label nameLabel;

    @FXML
    private Label titleLabel;

    @FXML
    private Label messageLabel;

    /**
     * Handles the event when the add button is clicked.
     * Adds a new product to the database.
     * @param event The action event.
     */
    @FXML
    void addItem(ActionEvent event) {
        AbstractDAO<Product> productAbstractDAO = new ProductDAO();
        ProductBLL productBLL = new ProductBLL();
        try {
            String name = nameField.getText();
            int quantity = Integer.parseInt(quantityField.getText());
            Product product = new Product(productAbstractDAO.findLastId() + 1,name,quantity);
            productBLL.insert(product);
            messageLabel.setText("Product created");
        }catch (IllegalArgumentException ex){
            messageLabel.setText("Invalid Quantity");
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    /**
     * Handles the event when the back button is clicked.
     * Navigates back to the products page.
     * @param event The action event.
     */
    @FXML
    void goBack(ActionEvent event) {
        try {
            Parent root;
            Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            FXMLLoader fxmlLoader = new FXMLLoader(Main.class.getResource("products-page.fxml"));
            root = fxmlLoader.load();
            stage.setTitle("Warehouse - Oltean Razvan");
            stage.setScene(new Scene(root));
            stage.show();
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
}
