package view;

import com.example.orders_management.Main;
import data.AbstractDAO;
import data.ProductDAO;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import model.Product;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * The ViewProductController class controls the functionality related to viewing products.
 */
public class ViewProductController implements Initializable {

    @FXML
    private Button backButton;

    @FXML
    private TextField searchField;

    @FXML
    private Label searchLabel;

    @FXML
    private TableView<Product> table;

    @FXML
    private TableColumn<Product, Integer> tableQuantity;

    @FXML
    private TableColumn<Product, Integer> tableId;

    @FXML
    private TableColumn<Product, String> tableName;

    @FXML
    private Label titleLabel;

    private String executeQuery;

    /**
     * Navigates back to the products page.
     * @param event The action event.
     */
    @FXML
    void goBack(ActionEvent event) {
        try {
            Parent root;
            Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            FXMLLoader fxmlLoader = new FXMLLoader(Main.class.getResource("products-page.fxml"));
            root = fxmlLoader.load();
            stage.setTitle("Warehouse - Oltean Razvan");
            stage.setScene(new Scene(root));
            stage.show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Searches for products based on the entered text in the search field.
     * @param event The action event.
     */
    @FXML
    void searchAction(ActionEvent event) {
        String searchBox = searchField.getText();
        if(!searchBox.isEmpty())
            updateTable(searchBox);
        else
            updateTable();
    }

    /**
     * Updates the product table.
     */
    public void updateTable() {
        tableId.setCellValueFactory(new PropertyValueFactory<>("id"));
        tableName.setCellValueFactory(new PropertyValueFactory<>("name"));
        tableQuantity.setCellValueFactory(new PropertyValueFactory<>("quantity"));
        ObservableList<Product> tempList = FXCollections.observableArrayList();
        AbstractDAO<Product> productAbstractDAO = new ProductDAO();
        try {
            tempList.setAll(productAbstractDAO.findAll());
            table.setItems(tempList);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    /**
     * Updates the table view with the clients based on the current query.
     * @param searchBox searches for the elements containing that specific String
     */
    public void updateTable(String searchBox) {
        tableId.setCellValueFactory(new PropertyValueFactory<>("id"));
        tableName.setCellValueFactory(new PropertyValueFactory<>("name"));
        tableQuantity.setCellValueFactory(new PropertyValueFactory<>("quantity"));
        AbstractDAO<Product> productAbstractDAO = new ProductDAO();
        ObservableList<Product> tempList = FXCollections.observableArrayList();
        try {
            tempList.setAll(productAbstractDAO.findAll(searchBox));
            table.setItems(tempList);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Initializes the view product controller.
     * @param url The URL to initialize.
     * @param resourceBundle The resource bundle associated with the controller.
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        executeQuery = "select * from view_product";
        updateTable();
    }
}
