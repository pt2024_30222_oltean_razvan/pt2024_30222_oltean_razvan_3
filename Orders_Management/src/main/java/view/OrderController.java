package view;

import com.example.orders_management.Main;
import data.*;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import logic.OrderBLL;
import logic.validators.UnderstockException;
import model.Client;
import model.Order;
import model.Product;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

/**
 * The OrderController class controls the functionality of placing an order.
 */
public class OrderController implements Initializable {
    @FXML
    private Button backButton;

    @FXML
    private ChoiceBox<String> clientBox = new ChoiceBox<>();

    @FXML
    private Label clientLabel;

    @FXML
    private Button orderButton;

    @FXML
    private Label orderLabel;

    @FXML
    private Label messageLabel;

    @FXML
    private ChoiceBox<String> productBox = new ChoiceBox<>();

    @FXML
    private TextField priceField;

    @FXML
    private Label priceLabel;

    @FXML
    private Label productLabel;

    @FXML
    private TextField quantityField;

    @FXML
    private Label quantityLabel;

    @FXML
    private ImageView viewButton;

    private AbstractDAO<Client> clientAbstractDAO = new ClientDAO();
    private AbstractDAO<Product> productAbstractDAO = new ProductDAO();

    /**
     * Handles the event when the view button is clicked.
     * Navigates to the view order page.
     *
     * @param event The mouse event.
     */
    @FXML
    void viewAction(MouseEvent event) {
        try {
            Parent root;
            Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            FXMLLoader fxmlLoader = new FXMLLoader(Main.class.getResource("view-order.fxml"));
            root = fxmlLoader.load();
            stage.setTitle("Warehouse - Oltean Razvan");
            stage.setScene(new Scene(root));
            stage.show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Handles the event when the back button is clicked.
     * Navigates back to the main page.
     *
     * @param event The action event.
     */
    @FXML
    void goBack(ActionEvent event) {
        try {
            Parent root;
            Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            FXMLLoader fxmlLoader = new FXMLLoader(Main.class.getResource("main-page.fxml"));
            root = fxmlLoader.load();
            stage.setTitle("Warehouse - Oltean Razvan");
            stage.setScene(new Scene(root));
            stage.show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Handles the event when the order button is clicked.
     * Places an order with the selected client and product.
     *
     * @param event The action event.
     */
    @FXML
    void placeOrder(ActionEvent event) {
        try {
            AbstractDAO<Order> orderAbstractDAO = new OrderDAO();
            AbstractDAO<Client> clientAbstractDAO = new ClientDAO();
            AbstractDAO<Product> productAbstractDAO = new ProductDAO();
            OrderBLL orderBLL = new OrderBLL();
            Order order = new Order();
            order.setId(orderAbstractDAO.findLastId() + 1);
            Client client = clientAbstractDAO.findByName(clientBox.getValue());
            Product product = productAbstractDAO.findByName(productBox.getValue());
            order.setClientId(client.getId());
            order.setProductId(product.getId());
            order.setClientName(client.getName());
            order.setProductName(product.getName());
            order.setQuantity(Integer.parseInt(quantityField.getText()));
            order.setBill(order.getId(), Integer.parseInt(priceField.getText()));
            orderBLL.insert(order, product.getQuantity());
            messageLabel.setText("Order set");
        } catch (IllegalArgumentException ex) {
            messageLabel.setText("Invalid Field");
        } catch (UnderstockException ex) {
            messageLabel.setText("Understock");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Initializes the controller after its root element has been completely processed.
     * This method is automatically called after the FXML file has been loaded.
     *
     * @param url the location used to resolve relative paths for the root object, or null if the location is not known
     * @param resourceBundle the resources used to localize the root object, or null if the root object was not localized
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        List<Client> clientList = clientAbstractDAO.findAll();
        List<Product> productList = productAbstractDAO.findAll();
        String[] clientOptions = new String[clientAbstractDAO.findSize()];
        String[] productOptions = new String[productAbstractDAO.findSize()];
        int i = 0, j = 0;
        try {
            for (Client c : clientList) {
                clientOptions[i++] = c.getName();
            }
            for (Product p : productList) {
                productOptions[j++] = p.getName();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        clientBox.getItems().addAll(clientOptions);
        productBox.getItems().addAll(productOptions);
    }
}
