package view;

import com.example.orders_management.Main;
import data.AbstractDAO;
import data.ProductDAO;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import model.Product;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

/**
 * The EditProductController class controls the functionality of editing a product.
 */
public class EditProductController implements Initializable {

    @FXML
    private Button applyButton;

    @FXML
    private Button backButton;

    @FXML
    private TextField quantityField;

    @FXML
    private Label quantityLabel;

    @FXML
    private TextField nameField;

    @FXML
    private Label nameLabel;

    @FXML
    private ChoiceBox<String> selectBox = new ChoiceBox<>();

    @FXML
    private Label selectLabel;

    @FXML
    private Label titleLabel;
    @FXML
    private Label messageLabel;
    private AbstractDAO<Product> productAbstractDAO = new ProductDAO();
    /**
     * Handles the event when the apply button is clicked.
     * Applies changes to the selected product in the database.
     *
     * @param event The action event.
     */
    @FXML
    void applyChanges(ActionEvent event) {
        try{
            String selectedOption = selectBox.getValue();
            String productName = nameField.getText();
            int productQuantity = Integer.parseInt(quantityField.getText());
            Product product = productAbstractDAO.findByName(selectedOption);
            product.setName(productName);
            product.setQuantity(productQuantity);
            productAbstractDAO.update(product);
            updateBox();
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
    /**
     * Handles the event when the back button is clicked.
     * Navigates back to the products page.
     *
     * @param event The action event.
     */
    @FXML
    void goBack(ActionEvent event) {
        try {
            Parent root;
            Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            FXMLLoader fxmlLoader = new FXMLLoader(Main.class.getResource("products-page.fxml"));
            root = fxmlLoader.load();
            stage.setTitle("Warehouse - Oltean Razvan");
            stage.setScene(new Scene(root));
            stage.show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    private void updateBox(){
        selectBox.getItems().setAll();
        List<Product> list = productAbstractDAO.findAll();
        String[] options = new String[productAbstractDAO.findSize()];
        int j = 0;
        try{
            for(Product p: list){
                options[j++] = p.getName();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
        selectBox.getItems().addAll(options);
    }
    /**
     * Initializes the controller after its root element has been completely processed.
     * This method is automatically called after the FXML file has been loaded.
     *
     * @param url the location used to resolve relative paths for the root object, or null if the location is not known
     * @param resourceBundle the resources used to localize the root object, or null if the root object was not localized
     */
    public void initialize(URL url, ResourceBundle resourceBundle) {
        updateBox();
    }
}
