package view;

import com.example.orders_management.Main;
import data.AbstractDAO;
import data.ClientDAO;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import model.Client;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * The ViewClientController class controls the functionality related to viewing clients.
 */
public class ViewClientController implements Initializable {

    @FXML
    private Button backButton;

    @FXML
    private TextField searchField;

    @FXML
    private Label searchLabel;

    @FXML
    private TableView<Client> table = new TableView<>();

    @FXML
    private TableColumn<Client, String> tableEmail = new TableColumn<>("Email");

    @FXML
    private TableColumn<Client, Integer> tableId = new TableColumn<>("ID");

    @FXML
    private TableColumn<Client, String> tableName = new TableColumn<>("Name");

    @FXML
    private Label titleLabel;
    /**
     * Navigates back to the clients page.
     * @param event The action event.
     */
    @FXML
    void goBack(ActionEvent event) {
        try {
            Parent root;
            Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            FXMLLoader fxmlLoader = new FXMLLoader(Main.class.getResource("clients-page.fxml"));
            root = fxmlLoader.load();
            stage.setTitle("Warehouse - Oltean Razvan");
            stage.setScene(new Scene(root));
            stage.show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Searches for clients based on the entered text in the search field.
     * @param event The action event.
     */
    @FXML
    void searchAction(ActionEvent event) {
        String searchBox = searchField.getText();
        if(!searchBox.isEmpty())
            updateTable(searchBox);
        else
            updateTable();
    }

    /**
     * Updates the table view with the clients based on the current query.
     */
    public void updateTable() {
        tableId.setCellValueFactory(new PropertyValueFactory<>("id"));
        tableName.setCellValueFactory(new PropertyValueFactory<>("name"));
        tableEmail.setCellValueFactory(new PropertyValueFactory<>("email"));
        ObservableList<Client> tempList = FXCollections.observableArrayList();
        AbstractDAO<Client> clientAbstractDAO = new ClientDAO();
        try {
            tempList.setAll(clientAbstractDAO.findAll());
            table.setItems(tempList);
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    /**
     * Updates the table view with the clients based on the current query.
     * @param searchBox searches for the elements containing that specific String
     */
    public void updateTable(String searchBox) {
        tableId.setCellValueFactory(new PropertyValueFactory<>("id"));
        tableName.setCellValueFactory(new PropertyValueFactory<>("name"));
        tableEmail.setCellValueFactory(new PropertyValueFactory<>("email"));
        AbstractDAO<Client> clientAbstractDAO = new ClientDAO();
        ObservableList<Client> tempList = FXCollections.observableArrayList();
        try {
            tempList.setAll(clientAbstractDAO.findAll(searchBox));
            table.setItems(tempList);
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Initializes the view client controller.
     * @param url The URL to initialize.
     * @param resourceBundle The resource bundle associated with the controller.
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        updateTable();
    }
}
