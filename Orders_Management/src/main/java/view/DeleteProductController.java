package view;

import com.example.orders_management.Main;
import data.AbstractDAO;
import data.ProductDAO;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import model.Product;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

/**
 * The DeleteProductController class controls the functionality of the Delete Product view.
 */
public class DeleteProductController implements Initializable {

    @FXML
    private Button backButton;

    @FXML
    private Button deleteButton;

    @FXML
    private ChoiceBox<String> selectBox = new ChoiceBox<>();

    @FXML
    private Label selectLabel;

    @FXML
    private Label titleLabel;
    private AbstractDAO<Product> productAbstractDAO = new ProductDAO();
    @FXML
    void deleteItem(ActionEvent event) {
        try{
            String name = selectBox.getValue();
            Product product = productAbstractDAO.findByName(name);
            productAbstractDAO.delete(product);
            updateBox();
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    /**
     * Handles the event when the back button is clicked.
     * Navigates back to the products page.
     * @param event The action event.
     */
    @FXML
    void goBack(ActionEvent event) {
        try {
            Parent root;
            Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            FXMLLoader fxmlLoader = new FXMLLoader(Main.class.getResource("products-page.fxml"));
            root = fxmlLoader.load();
            stage.setTitle("Warehouse - Oltean Razvan");
            stage.setScene(new Scene(root));
            stage.show();
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
    private void updateBox(){
        selectBox.getItems().setAll();
        List<Product> list = productAbstractDAO.findAll();
        String[] options = new String[productAbstractDAO.findSize()];
        int j = 0;
        try{
            for(Product p: list){
                options[j++] = p.getName();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
        selectBox.getItems().addAll(options);
    }
    /**
     * Initializes the controller after its root element has been completely processed.
     * This method is automatically called after the FXML file has been loaded.
     *
     * @param url the location used to resolve relative paths for the root object, or null if the location is not known
     * @param resourceBundle the resources used to localize the root object, or null if the root object was not localized
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        updateBox();
    }
}
