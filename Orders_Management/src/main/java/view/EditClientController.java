package view;

import com.example.orders_management.Main;
import data.AbstractDAO;
import data.ClientDAO;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import model.Client;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

/**
 * The EditClientController class controls the functionality of editing a client.
 */
public class EditClientController implements Initializable {

    @FXML
    private Button applyButton;

    @FXML
    private Button backButton;

    @FXML
    private TextField emailField;

    @FXML
    private Label emailLabel;

    @FXML
    private TextField nameField;

    @FXML
    private Label nameLabel;

    @FXML
    private ChoiceBox<String> selectBox = new ChoiceBox<>();

    @FXML
    private Label selectLabel;

    @FXML
    private Label titleLabel;
    @FXML
    private Label messageLabel;
    private AbstractDAO<Client> clientAbstractDAO = new ClientDAO();
    @FXML
    void applyChanges(ActionEvent event) {
        try{
            String selectedOption = selectBox.getValue();
            String clientName = nameField.getText();
            String clientEmail = emailField.getText();
            Client client = clientAbstractDAO.findByName(selectedOption);
            client.setName(clientName);
            client.setEmail(clientEmail);
            clientAbstractDAO.update(client);
            updateBox();
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    /**
     * Handles the event when the back button is clicked.
     * Navigates back to the clients page.
     * @param event The action event.
     */
    @FXML
    void goBack(ActionEvent event) {
        try {
            Parent root;
            Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            FXMLLoader fxmlLoader = new FXMLLoader(Main.class.getResource("clients-page.fxml"));
            root = fxmlLoader.load();
            stage.setTitle("Warehouse - Oltean Razvan");
            stage.setScene(new Scene(root));
            stage.show();
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
    private void updateBox(){
        selectBox.getItems().setAll();
        List<Client> list = clientAbstractDAO.findAll();
        String[] options = new String[clientAbstractDAO.findSize()];
        int j = 0;
        try{
            for(Client c: list){
                options[j++] = c.getName();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
        selectBox.getItems().addAll(options);
    }
    /**
     * Initializes the controller after its root element has been completely processed.
     * This method is automatically called after the FXML file has been loaded.
     *
     * @param url the location used to resolve relative paths for the root object, or null if the location is not known
     * @param resourceBundle the resources used to localize the root object, or null if the root object was not localized
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        updateBox();
    }
}
