package view;

import com.example.orders_management.Main;
import data.AbstractDAO;
import data.ClientDAO;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import logic.ClientBLL;
import model.Client;

/**
 * The AddClientController class controls the functionality of the Add Client view.
 */
public class AddClientController {

    @FXML
    private Button addButton;

    @FXML
    private Button backButton;

    @FXML
    private TextField emailField;

    @FXML
    private Label emailLabel;

    @FXML
    private TextField nameField;

    @FXML
    private Label nameLabel;

    @FXML
    private Label titleLabel;

    @FXML
    private Label messageLabel;
    /**
     * Handles the event when the add button is clicked.
     * Adds a new client to the database.
     * @param event The action event.
     */
    @FXML
    void addItem(ActionEvent event) {
        AbstractDAO<Client> clientAbstractDAO = new ClientDAO();
        ClientBLL clientBLL = new ClientBLL();
        try {
            String name = nameField.getText();
            String email = emailField.getText();
            Client client = new Client(clientAbstractDAO.findLastId() + 1,name,email);
            clientBLL.insert(client);
            messageLabel.setText("Client created");
        }catch (IllegalArgumentException ex){
            messageLabel.setText("Invalid Email");
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    /**
     * Handles the event when the back button is clicked.
     * Navigates back to the clients page.
     * @param event The action event.
     */
    @FXML
    void goBack(ActionEvent event) {
        try {
            Parent root;
            Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            FXMLLoader fxmlLoader = new FXMLLoader(Main.class.getResource("clients-page.fxml"));
            root = fxmlLoader.load();
            stage.setTitle("Warehouse - Oltean Razvan");
            stage.setScene(new Scene(root));
            stage.show();
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
}
