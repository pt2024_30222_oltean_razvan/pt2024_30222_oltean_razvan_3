package view;

import com.example.orders_management.Main;
import data.AbstractDAO;
import data.OrderDAO;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import javafx.util.Callback;
import model.Order;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * The ViewOrderController class controls the functionality related to viewing orders.
 */
public class ViewOrderController implements Initializable {

    @FXML
    private Button backButton;

    @FXML
    private TableView<Order> table;

    @FXML
    private TableColumn<Order, String> tableClient;

    @FXML
    private TableColumn<Order, Double> tablePrice;

    @FXML
    private TableColumn<Order, String> tableProduct;

    @FXML
    private TableColumn<Order, Integer> tableQuantity;

    @FXML
    private Label titleLabel;

    /**
     * Navigates back to the orders page.
     * @param event The action event.
     */
    @FXML
    void goBack(ActionEvent event) {
        try {
            Parent root;
            Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            FXMLLoader fxmlLoader = new FXMLLoader(Main.class.getResource("orders-page.fxml"));
            root = fxmlLoader.load();
            stage.setTitle("Warehouse - Oltean Razvan");
            stage.setScene(new Scene(root));
            stage.show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Initializes the view order controller.
     * @param url The URL to initialize.
     * @param resourceBundle The resource bundle associated with the controller.
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        tableClient.setCellValueFactory(new PropertyValueFactory<>("clientName"));
        tableProduct.setCellValueFactory(new PropertyValueFactory<>("productName"));
        tableQuantity.setCellValueFactory(new PropertyValueFactory<>("quantity"));
        tablePrice.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Order, Double>, ObservableValue<Double>>() {
            @Override
            public ObservableValue<Double> call(TableColumn.CellDataFeatures<Order, Double> orderDoubleCellDataFeatures) {
                return new SimpleDoubleProperty(orderDoubleCellDataFeatures.getValue().getPrice()).asObject();
            }
        });
        AbstractDAO<Order> orderAbstractDAO = new OrderDAO();
        ObservableList<Order> tempList = FXCollections.observableArrayList();
        try {
            tempList.setAll(orderAbstractDAO.findAll());
            table.setItems(tempList);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
