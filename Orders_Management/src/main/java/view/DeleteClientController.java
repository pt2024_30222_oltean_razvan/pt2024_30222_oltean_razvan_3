package view;

import com.example.orders_management.Main;
import data.AbstractDAO;
import data.ClientDAO;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import model.Client;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

/**
 * The DeleteClientController class controls the functionality of the Delete Client view.
 */
public class DeleteClientController implements Initializable {

    @FXML
    private Button backButton;

    @FXML
    private Button deleteButton;

    @FXML
    private ChoiceBox<String> selectBox = new ChoiceBox<>();

    @FXML
    private Label selectLabel;

    @FXML
    private Label titleLabel;
    private AbstractDAO<Client> clientAbstractDAO = new ClientDAO();
    /**
     * Handles the event when the delete button is clicked.
     * Deletes the selected client from the database.
     * @param event The action event.
     */
    @FXML
    void deleteItem(ActionEvent event) {
        try{
            String name = selectBox.getValue();
            Client client = clientAbstractDAO.findByName(name);
            clientAbstractDAO.delete(client);
            updateBox();
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    /**
     * Handles the event when the back button is clicked.
     * Navigates back to the clients page.
     * @param event The action event.
     */
    @FXML
    void goBack(ActionEvent event) {
        try {
            Parent root;
            Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            FXMLLoader fxmlLoader = new FXMLLoader(Main.class.getResource("clients-page.fxml"));
            root = fxmlLoader.load();
            stage.setTitle("Warehouse - Oltean Razvan");
            stage.setScene(new Scene(root));
            stage.show();
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
    private void updateBox(){
        selectBox.getItems().setAll();
        List<Client> list = clientAbstractDAO.findAll();
        String[] options = new String[clientAbstractDAO.findSize()];
        int j = 0;
        try{
            for(Client c: list){
                options[j++] = c.getName();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
        selectBox.getItems().addAll(options);
    }
    /**
     * Initializes the controller after its root element has been completely processed.
     * This method is automatically called after the FXML file has been loaded.
     *
     * @param url the location used to resolve relative paths for the root object, or null if the location is not known
     * @param resourceBundle the resources used to localize the root object, or null if the root object was not localized
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        updateBox();
    }
}
